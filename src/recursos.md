---
title: Recursos
description: Recursos y Herramientas
layout: layout.html
---
# Recursos y Herramientas

Muchos de los programas originales de la época están disponibles en páginas de abandonware y pueden
ser muy útiles para recuperar el espíritu de 1997. Aquí dejo unos cuantos enlaces a programas y
recursos que creo que pueden resultarte útiles.

## DIV Games Studio

Aquí podéis descargas ISOs de las dos versiones de DIV en castellano:

- [DIV Games Studio][div1-download]
- [DIV 2 Games Studio][div2-download]

## DIV manias

En las revistas que aparecieron poco antes de la salida de DIV 2 podréis encontrar un montón de recursos
de todo tipo, desde gráficos y texturas, a programas de toda índole: editores 3D, retoque fotográfico,
trackers, editores de audio, etc.

| Revista                 | CD                    |
|-------------------------|-----------------------|
| [Número 1][divmania-1]  | [CD 1][divmania-cd-1] |
| [Número 2][divmania-2]  | [CD 2][divmania-cd-2] |
| [Número 3][divmania-3]  | [CD 3][divmania-cd-3] |
| [Número 4][divmania-4]  | [CD 4][divmania-cd-4] |
| [Número 5][divmania-5]  | [CD 5][divmania-cd-5] |
| [Número 6][divmania-6]  | [CD 6][divmania-cd-6] |
| [Número 7][divmania-7]  | [CD 7][divmania-cd-7] |
| [Número 8][divmania-8]  | [CD 8][divmania-cd-8] |
| [Número 9][divmania-9]  | [CD 9][divmania-cd-9] |

## Gráficos

Programas de dibujo de la época:

- [Deluxe Paint Animation][deluxe-paint-download]
- [Autodesk Animator Pro][autodesk-animator-download]

Algunos programas más modernos:

- [Blender][blender]
- [GIMP][gimp]
- [Inkscape][inkscape]

Web para descargar sonidos de dominio público:

- [OpenGameArt][OpenGameArt-2d]

## Música

Aquí os dejo una lista de los trackers más famosos de la época:

- [ScreamTracker][scream-tracker-download]
- [ImpulseTracker][impulse-tracker-download]
- [FastTracker][fast-tracker-download]

Y aquí algunos más nuevos:

- [MilkyTracker][milky-tracker]
- [SchismTracker][schism-tracker]

Colección de .XMs, .ITs y .MODs:

- [The Mod Archive][mod-archive]
- [Tekno Music Mania 1998][tekno-music-mania-download]

## Samples y sonidos

Torrents de audios:

- [KIArchive][kiarchive]
- [Waveworld][waveworld]
- [Woolyss's chiptune samples][woolyss]

Web para descargar sonidos de dominio público:

- [FreeSound][freesound]
- [OpenGameArt][OpenGameArt-audio]

## Otros

- [MS-DOS 6.22][msdos-download]

[div1-download]: https://drive.google.com/open?id=1zkSkK-F_H01oDxuE9Sw8zflwE17GdZAY
[div2-download]: https://drive.google.com/open?id=1Qc7IgcHU4cReJM0NSeCVKxUF1eUpA6mF

[divmania-1]: https://drive.google.com/open?id=15zX6-80Hykm7lC3C2iuLv1zlIPHhUbgs
[divmania-2]: https://drive.google.com/open?id=1I-KcwHECSvV0Um-Rxrhbf0hjmfqltz5Z
[divmania-3]: https://drive.google.com/open?id=1EzLMRuSLuLutij-S-ukS64Ti03rWgegF
[divmania-4]: https://drive.google.com/open?id=1GWnw580DGQ2UYbFfeHWbRiWXjxQcPgr2
[divmania-5]: https://drive.google.com/open?id=1EUHooS0TKgTR6gOK0Xx3Kcz1_OkqBJV0
[divmania-6]: https://drive.google.com/open?id=1tCwTD1nnBndx-0ARTnB9SGZnAMDVLH5K
[divmania-7]: https://drive.google.com/open?id=1cARE2_RNtlY0RcsxArc2FwmfE0Tlyf2r
[divmania-8]: https://drive.google.com/open?id=1KKOfye2IGGfaHHL_B850tC55GIdGSdok
[divmania-9]: https://drive.google.com/open?id=1EW0qDkOHpyvjk7O2YbcNKDoxtqVDkket

[divmania-cd-1]: _blank
[divmania-cd-2]: https://drive.google.com/open?id=1BTYsxgRW-c9187l_olAo2j7NO21dmHxW
[divmania-cd-3]: https://drive.google.com/open?id=1-gy9SfQmOneIWpJUzceRB3zqUX_-W3e4
[divmania-cd-4]: https://drive.google.com/open?id=190Ar4ZZg2tXyimMok-vsyDQ_uDRby16-
[divmania-cd-5]: https://drive.google.com/open?id=1P0of2gRoS-RR8YAMjeFXzW39MDtgLwWL
[divmania-cd-6]: https://drive.google.com/open?id=1-KEs2u1ogD6WlL_hQH2qUxyW8sbNgW7p
[divmania-cd-7]: https://drive.google.com/open?id=1f6hwqhn-2yIrkCjV5Ys4OtECu5XY3gTA
[divmania-cd-8]: https://drive.google.com/open?id=1ziJadvfydoOn242b63EsNgVe82dzgK6O
[divmania-cd-9]: https://drive.google.com/open?id=16uEbQhaZtEMHfrclnmo6XjdvtOJr0lvP

[msdos-download]: https://archive.org/details/MSDOS6.22_201905

[scream-tracker-download]: https://archive.org/details/scrmt321
[impulse-tracker-download]: https://archive.org/details/it212_zip
[fast-tracker-download]: https://archive.org/details/demoscene_Fasttracker2-Triton
[mod-archive]: https://modarchive.org/
[tekno-music-mania-download]: https://archive.org/details/tekno-1998-music-mania

[milky-tracker]: https://milkytracker.titandemo.org/
[schism-tracker]: http://schismtracker.org/

[kiarchive]: http://tracker.modarchive.org/torrents/kiarchive.zip.torrent
[waveworld]: http://tracker.modarchive.org/torrents/TMA-waveworld.zip.torrent
[woolyss]: http://tracker.modarchive.org/torrents/woolyss-chiptune-samples.zip.torrent

[freesound]: https://freesound.org/
[OpenGameArt-audio]: https://opengameart.org/art-search-advanced?keys=&field_art_type_tid%5B%5D=13&sort_by=count&sort_order=DESC
[OpenGameArt-2d]: https://opengameart.org/art-search-advanced?keys=&field_art_type_tid%5B%5D=9&sort_by=count&sort_order=DESC

[deluxe-paint-download]: https://classicreload.com/dosx-deluxe-paint-animation.html
[autodesk-animator-download]: https://drive.google.com/open?id=19wlPOv5iiwY4HeT4PzwYpniNu3VKLm8v

[blender]: https://www.blender.org/
[gimp]: https://www.gimp.org/
[inkscape]: https://inkscape.org/
